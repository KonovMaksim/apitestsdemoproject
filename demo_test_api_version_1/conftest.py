import requests
import pytest


class ApiClient:
    def __init__(self, address_host):
        self.address_host = address_host

    def get(self, path="/", params=None, headers=None):
        url = f"{self.address_host}{path}"
        return requests.get(url=url, params=params, headers=headers)

    def post(self, path="/", params=None, data=None, json=None, headers=None):
        url = f"{self.address_host}{path}"
        return requests.post(url=url, params=params, data=data, json=json, headers=headers)


@pytest.fixture
def reqres_api():
    return ApiClient(address_host="https://reqres.in/")
