import allure


def test_list_users(reqres_api, end_point="api/users?page=2"):
    with allure.step(f"Send request {end_point}"):
        response = reqres_api.get(end_point)
        per_page = response.json()["per_page"]
        user_all_list = list(response.json()["data"])
    with allure.step("Checking the status code"):
        assert response.status_code == 200
    with allure.step("Checking response json"):
        assert response.json()["page"] == 2
        assert len(user_all_list) == per_page
        for data in user_all_list:
            print(data["id"])

